# Tikee
Platform voor het kopen en verkopen van tickets voor evenementen en bijeenkomsten.

## Auteurs
* Sander Van Damma
* Shaun Janssens

New Media Design & Development II  
Bachelor in de grafische en digitale media 2015-2016  
Multimediaproductie proDEV Arteveldehogeschool  

## Live version
Frontend: [www.tikee.be](http://www.tikee.be)
Backend: [www.tikee.be:3000](http://www.tikee.be:3000)

# Deploy

## Frontend
1. Naar "app" map navigeren en "npm install" uitvoeren voor de nodige plugins en data.
2. Daarna "gulp" uitvoeren voor alles klaar te zetten en de website te openen in localhost.

## Backend
1. Naar "www" navigeren en "composer update" uitvoeren vor de nodige plugins en data.
2. Om de backend weer te geven: "php artisan serve", deze lanceerd de website op "localhost:8000".
3. Om de database aan te maken en te vullen voert u "php artisan migrate" uit en daarna "php artisan db:seed" (deze duurt even),

## Login backend
Gebruikersnaam: admin  
Wachtwoord: tikee