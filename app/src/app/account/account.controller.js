/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.account').controller('AccountController', AccountController);

    AccountController.$inject = ['$log', '$scope', '$http'];

    function AccountController($log, $scope, $http)
    {
        var vm = this;
        vm.$$ui = { classname: 'account' }

        var user_data = JSON.parse(localStorage.getItem('user_data'));

        $scope.first_name = user_data.first_name;
        $scope.last_name = user_data.last_name;

        $http.get('http://95.85.26.250:3000/api/events/user/' + user_data.id).success(function(dataEvents) {
            
            $scope.user_events = dataEvents.length;
            $scope.mijn_events = dataEvents;

        });

        $http.get('http://95.85.26.250:3000/api/tickets/user/' + user_data.id).success(function(dataTickets) {
            
            var ticketsAmount = 0;
            for (var i = 0; i < dataTickets.length; i++)
                ticketsAmount += dataTickets[i].amount;
            
            $scope.coming_events = dataTickets.length;
            $scope.user_tickets = ticketsAmount;
            $scope.mijn_tickets = dataTickets;

        });

        $scope.avatar = user_data.avatar;

        $scope.updateName = function() {
            if (document.querySelector('input[name=first_name]').value != "" && document.querySelector('input[name=last_name]').value != "")
            {
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "http://95.85.26.250:3000/api/users/" + user_data.id + "/update",
                    "method": "POST",
                    "headers": {
                        "content-type": "application/x-www-form-urlencoded"
                    },
                    "data": {
                        "last_name": document.querySelector('input[name=last_name]').value,
                        "first_name": document.querySelector('input[name=first_name]').value
                    }
                }

                $.ajax(settings).done(function (response) { 
                    location.reload();
                });
            }
        }

        $scope.uploadAvatar = function() {
            if (document.querySelector('input[type=file]').value != "")
            {
                var form = new FormData();
                form.append("avatar", document.querySelector('input[type=file]').files[0]);

                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "contentType": false,
                    "processData": false,
                    "mimeType": "multipart/form-data",
                    "url": "http://www.tikee.be:3000/api/users/" + user_data.id + "/avatar", 
                    "method": "POST",
                    "data": form
                }

                $.ajax(settings).done(function (response) {
                    location.reload();
                }); 
            }
        }

        function uploadFile(file){

            var url = '';
            var xhr = new XMLHttpRequest();
            var fd = new FormData();
            xhr.open("POST", url, true);

            xhr.onreadystatechange = function()
            {
                if (xhr.readyState == 4 && xhr.status == 200)
                {
                    console.log(xhr.responseText);
                }
            };

            fd.append("upload_file", file);
            xhr.send(fd);
        }

        $scope.makeEvent = function()
        {
            event.preventDefault();

            if (document.querySelector('input[type=file]').value != "")
            {
                var form = new FormData();
                form.append("user_id", JSON.parse(localStorage.getItem('user_data')).id);
                form.append("name", document.querySelector('input[name=name]').value);
                form.append("datetime", document.querySelector('input[name=date]').value);
                form.append("time", document.querySelector('input[name=time]').value);
                form.append("price", document.querySelector('input[name=price]').value);
                form.append("description", document.querySelector('textarea[name=description]').value);
                form.append("website_url", document.querySelector('input[name=website]').value);
                form.append("keywords", document.querySelector('input[name=keywords]').value);
                form.append("address", document.querySelector('input[name=address]').value);
                form.append("image", document.querySelector('input[type=file]').files[0]);

                var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "http://www.tikee.be:3000/api/events/add",
                  "method": "POST",
                  "processData": false,
                  "contentType": false,
                  "mimeType": "multipart/form-data",
                  "data": form
                }

                $.ajax(settings).done(function (response) {
                    location.reload();
                });
            }
        } 
    }

})();
