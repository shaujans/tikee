/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.style-guide')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('style-guide', {
                cache: false, // false will reload on every visit.
                controller: 'StyleGuideController as vm',
                templateUrl: 'html/style-guide/style-guide.view.html',
                url: '/style-guide'
            });
    }

})();