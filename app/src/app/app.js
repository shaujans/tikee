/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    // Module declarations
    angular.module('app', [
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',
        'ui.router',

        'app.blog',
        'app.home',
        'app.events',
        'app.about',
        'app.account',
        'app.search',
        'app.style-guide'
    ]).run(function($rootScope) {

        // Refresh user data
        if (localStorage.getItem('user_data') != null)
        {
            $.ajax({
                "async": true,
                "crossDomain": true,
                "url": "http://95.85.26.250:3000/api/users/" + JSON.parse(localStorage.getItem('user_data')).id,
                "method": "GET"
            }).done(function (response) {
                localStorage.setItem("user_data", JSON.stringify({
                    'id': response[0].id,
                    'name': response.username,
                    'first_name': response[0].first_name,
                    'last_name': response[0].last_name,
                    'email': response[0].email,
                    'avatar': response[0].avatar
                }));
            });
        }

        $rootScope.hide_black = function(){

            for (var i = 0; i < document.querySelectorAll(".blacks").length; i++)
            {
                document.querySelectorAll(".blacks")[i].style.display = "none";
                console.log(document.querySelectorAll(".blacks")[i].style.display);
            }

            document.getElementById("black").style.display = "none";

        };

        $rootScope.login = function()
        {
            event.preventDefault();
            console.log('login');

            document.getElementById("black").style.display = "block";
            document.getElementById("login").style.display = "block";
            document.getElementById("register").style.display = "none";
        }

        $rootScope.logout = function()
        {
            event.preventDefault();
            console.log('logout');

            localStorage.removeItem('user_data');
            location.reload();
        }

        $rootScope.register = function()
        {
            event.preventDefault();
            console.log('register');

            document.getElementById("black").style.display = "block";
            document.getElementById("login").style.display = "none";
            document.getElementById("register").style.display = "block";
        }

        $rootScope.register_user = function()
        {
            if (document.querySelector("input[name=register_email]").value != "" &&
                document.querySelector("input[name=register_password]").value != "" &&
                document.querySelector("input[name=register_confirm_password]").value != "" &&
                document.querySelector("input[name=register_first_name]").value != "" &&
                document.querySelector("input[name=register_last_name]").value != "" &&
                document.querySelector("input[name=register_username]").value != "" )
            {
                if (document.querySelector("input[name=register_password]").value == document.querySelector("input[name=register_confirm_password]").value)
                {
                    var settings = {
                        "crossDomain": true,
                        "url": "http://95.85.26.250:3000/api/register",
                        "method": "POST",
                        "headers": {
                            "content-type": "application/x-www-form-urlencoded"
                        },
                        "data": {
                            "email": document.querySelector("input[name=register_email]").value,
                            "password": document.querySelector("input[name=register_password]").value,
                            "first_name": document.querySelector("input[name=register_first_name]").value,
                            "last_name": document.querySelector("input[name=register_last_name]").value,
                            "username": document.querySelector("input[name=register_username]").value
                        }
                    }

                    $.ajax(settings).done(function (response) {

                        var form = new FormData();
                        form.append("username", document.querySelector("input[name=register_username]").value);
                        form.append("password", document.querySelector("input[name=register_password]").value);

                        var settings = {
                            "async": true,
                            "crossDomain": true,
                            "url": "http://www.tikee.be:3000/api/login",
                            "method": "POST",
                            "processData": false,
                            "contentType": false,
                            "mimeType": "multipart/form-data",
                            "data": form
                        }

                        $.ajax(settings).done(function (response) {
                            response = JSON.parse(response);
                            localStorage.setItem("user_data", JSON.stringify({
                                'id': response[0].id,
                                'name': response[0].username,
                                'first_name': response[0].first_name,
                                'last_name': response[0].last_name,
                                'email': response[0].email,
                                'avatar': response[0].avatar
                            }));

                            location.reload();
                        });

                    });
                }
            }
        }

        $rootScope.login_user = function()
        {
            event.preventDefault();

            var name_form = document.querySelector("input[name=login_username]");
            var password_form = document.querySelector("input[name=login_password]");

            if (name_form.value != "" && password_form.value != "")
            {
                var form = new FormData();
                form.append("username", name_form.value);
                form.append("password", password_form.value);

                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": "http://www.tikee.be:3000/api/login",
                    "method": "POST",
                    "processData": false,
                    "contentType": false,
                    "mimeType": "multipart/form-data",
                    "data": form
                }

                console.log(name_form.value, password_form.value);

                $.ajax(settings).done(function (response) {
                    if (response == "User not found")
                    {
                        name_form.style.border = "1px solid red";
                        password_form.style.border = "1px solid transparent";
                    }
                    else if (response == "Login failed")
                    {
                        name_form.style.border = "1px solid transparent";
                        password_form.style.border = "1px solid red";
                    }
                    else
                    {
                        name_form.style.border = "1px solid transparent";
                        password_form.style.border = "1px solid transparent";

                        response = JSON.parse(response);
                        localStorage.setItem("user_data", JSON.stringify({
                            'id': response[0].id,
                            'name': response[0].username,
                            'first_name': response[0].first_name,
                            'last_name': response[0].last_name,
                            'email': response[0].email,
                            'avatar': response[0].avatar
                        }));

                        location.reload();
                    }
                });
            }
        }

        $rootScope.$on('$viewContentLoaded', function() {

            if (localStorage.getItem("user_data") == null)
            {
                // NOT logged in
                var hide = document.querySelectorAll('.logged_in');
                var show = document.querySelectorAll('.not_logged_in');
                for (var i = 0; i < show.length; i++) show[i].style.display = "inline-block";
                for (var i = 0; i < hide.length; i++) hide[i].style.display = "none";
            }
            else
            {
                // Logged in
                var show = document.querySelectorAll('.logged_in');
                var hide = document.querySelectorAll('.not_logged_in');
                for (var i = 0; i < show.length; i++) show[i].style.display = "inline-block";
                for (var i = 0; i < hide.length; i++) hide[i].style.display = "none";
            }

        });

    });

    angular.module('app.blog', []);
    angular.module('app.home', []);
    angular.module('app.events', []);
    angular.module('app.about', []);
    angular.module('app.account', []);
    angular.module('app.search', []);
    angular.module('app.style-guide', []);

    function topmenuController() {

    }

    angular.module('app').component('menu', {
        templateUrl: 'html/components/topmenu.html',
        controller: topmenuController
    });

})();
