/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.events')
        .config(Routes);

    // Inject dependencies into constructor (needed when JS minification is applied).
    Routes.$inject = [
        // Angular
        '$stateProvider'
    ];

    function Routes(
        // Angular
        $stateProvider
    ) {
        $stateProvider
            .state('events', {
                cache: false, // false will reload on every visit.
                controller: 'EventsController as vm',
                templateUrl: 'html/events/events.view.html',
                url: '/events'
            })
            .state('event', {
                cache: false, // false will reload on every visit.
                controller: 'EventController as vm',
                templateUrl: 'html/events/event.view.html',
                url: '/events/{event_id:int}'
            });
    }

})();