/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.events')
        .controller('EventsController', EventsController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    EventsController.$inject = [
        // Angular
        '$log',
        '$scope',
        '$http'
    ];

    function EventsController(
        // Angular
        $log,
        $scope,
        $http
    ) {
        // ViewModel
        var vm = this;

        // User Interface
        vm.$$ui = {
            classname: 'events'
        };

        var data_to_fill = [];
        var per_click = 8;
        var data = [];
        var clicks = 1;

        $http.get('http://95.85.26.250:3000/api/events/desc').success(function(data_)
        {
            if (data_.length > per_click)
            {
                // Show more button
                $('.cta__more .button').css({"display": "block"});

                // Add the next entries
                for (var i = 0; i < per_click; i++)
                {
                    // Dont go out of index
                    if (i != data_.length)
                    {
                        data_to_fill.push(data_[i]);
                    }
                }

                // Fill datav
                $scope.events = data_to_fill;
            }
            else
            {
                // Hide more button
                $('.cta__more .button').css({"display": "none"});
                $scope.events = data_;
            }

            data = data_;
        });

        $scope.addEntries = addEntries;

        function addEntries(e) {

            e.preventDefault();
            clicks++;

            for (var i = per_click * (clicks - 1); i < per_click * clicks; i++) {
                // Dont go out of index
                if (i < data.length) {
                    console.log(i);
                    data_to_fill.push(data[i]);
                }
                else
                {
                    if (data.length <= document.querySelectorAll("article.event").length)
                    {
                        // Show more button
                        $('.cta__more .button').css({"display": "block"});
                    }
                    else
                    {
                        // Hide more button
                        $('.cta__more .button').css({"display": "none"});
                        $('.cta__more .end').css({"display": "block"});
                        $scope.events = data;
                    }
                }
            }

            $scope.events = data_to_fill;
        }

    }

})();


