/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.events')
        .controller('EventController', EventController);

    EventController.$inject = [ '$log', '$scope', '$rootScope', '$http', '$state' ];

    function EventController($log, $scope, $rootScope, $http, $state) { 

        var vm = this;
        vm.$$ui = { classname: 'events' }

        var data_to_fill = [];
        var params = { event_id: $state.params.event_id };
        var id_data = [];

        // Do JSON
        $http.get('http://95.85.26.250:3000/api/events/').success(function(data_) {

            for (var i = 0; i < 4; i++)
                data_to_fill.push(data_[(data_.length - 1) - i]);

            $scope.events = data_to_fill;
            $scope.event_detail = [data_to_fill[0]];

            // Get ID
            for (var i = 0; i < data_.length; i++) {
                if (data_[i].id == params.event_id)
                { 
                    $scope.event_detail = [data_[i]];
                    id_data = [data_[i]];
                }
            }

        });

        $http.get('http://95.85.26.250:3000/api/events/tickets/user/' + params.event_id).success(function(data) {
            
            if (data.length == 0)
            {
                console.log(document.querySelector('.peopleHide'));
                document.querySelector('.peopleHide').style.display = 'none';
            }
            else
            {
                for (var i = 0; i < data.length; i++)
                {
                    data[i].last_name_abbr = "";
                    for (var a = 0; a < data[i].last_name.split(" ").length; a++)
                    {
                        console.log(data[i].last_name.split(" ")[a]);
                        data[i].last_name_abbr += data[i].last_name.split(" ")[a][0] + ". ";
                    }
                }

                $scope.event_people = data;
            }

        });

        console.log(params.event_id);

        document.getElementById("black").style.display = "none";
        document.getElementById("thanks").style.display = "none";
        document.getElementById("buy").style.display = "none";

        $rootScope.hide_black();

        $scope.buy = function(id){
            event.preventDefault();
            document.getElementById("black").style.display = "block";
            document.getElementById("buy").style.display = "block";

            if (localStorage.getItem('user_data'))
            {
                var user_data = JSON.parse(localStorage.getItem('user_data'));

                document.querySelector("input[name=voornaam]").value = user_data.first_name;
                document.querySelector("input[name=achternaam]").value = user_data.last_name;
                document.querySelector("input[name=email]").value = user_data.email;
                document.querySelector("input[name=email_again]").value = user_data.email;

                document.querySelector("input[name=voornaam]").style.display = "none";
                document.querySelector("input[name=achternaam]").style.display = "none";
                document.querySelector("input[name=email]").style.display = "none";
                document.querySelector("input[name=email_again]").style.display = "none";
            }

            document.querySelector(".buy__header h1").innerHTML = id_data[0].name;
            document.querySelector(".buy__header p").innerHTML = id_data[0].datetime + " om " + id_data[0].time;

            document.querySelector(".thanks__header h1").innerHTML = id_data[0].name;
            document.querySelector(".thanks__header p").innerHTML = id_data[0].datetime + " om " + id_data[0].time;
        };

        $scope.buy_confirm = function(){
            event.preventDefault();

            var form_voornaam = document.querySelector(".buy__form input[name=voornaam]");
            var form_achternaam = document.querySelector(".buy__form input[name=achternaam]");
            var form_email = document.querySelector(".buy__form input[name=email]");
            var form_email_again = document.querySelector(".buy__form input[name=email_again]");
            var form_tickets = document.querySelector(".buy__form input[name=tickets]");

            if (form_voornaam.value != "" && form_achternaam.value != "" && form_email.value != "" && form_tickets.value != "" && form_email_again.value != "")
            {
                if (form_email.value == form_email_again.value)
                {
                    document.getElementById("buy").style.display = "none";
                    document.getElementById("thanks").style.display = "block";

                    document.querySelector(".thanks__text p").innerHTML = "Goed nieuws " + form_voornaam.value + ", je ticket is succesvol geregistreerd en je bent toegevoegd aan de gastenlijst voor '" + id_data[0].name + "'. Het adres is '" + id_data[0].address + "' en je wordt verwacht op " + id_data[0].datetime + " om " + id_data[0].time + ". Je hoeft geen tickets af te printen, je naam vermelden is genoeg!";
                    
                    // DO JSON POST
                    var settings = {
                        "async": true,
                        "crossDomain": true,
                        "url": "http://95.85.26.250:3000/api/tickets/add",
                        "method": "POST",
                        "headers": {
                            "content-type": "application/x-www-form-urlencoded"
                        },
                        "data": {
                            "user_id": JSON.parse(localStorage.getItem('user_data')).id,
                            "event_id": params.event_id,
                            "attend": "1",
                            "amount": form_tickets.value 
                        }
                    }

                    $.ajax(settings).done(function (response) {
                        location.reload();
                    });

                }
                else
                {
                    form_voornaam.style.border = "0px solid red";
                    form_achternaam.style.border = "0px solid red";
                    form_tickets.style.border = "0px solid red";

                    form_email.style.border = "1px solid red";
                    form_email_again.style.border = "1px solid red";
                }
            }
            else
            {
                if (form_voornaam.value == "") { form_voornaam.style.border = "1px solid red"; } else { form_voornaam.style.border = "0px solid red"; }
                if (form_achternaam.value == "") { form_achternaam.style.border = "1px solid red"; } else { form_achternaam.style.border = "0px solid red"; }
                if (form_email.value == "") { form_email.style.border = "1px solid red"; } else { form_email.style.border = "0px solid red"; }
                if (form_email_again.value == "") { form_email_again.style.border = "1px solid red"; } else { form_email_again.style.border = "0px solid red"; }
                if (form_tickets.value == "") { form_tickets.style.border = "1px solid red"; } else { form_tickets.style.border = "0px solid red"; }
            }

        };

        $scope.confirm = function(){
            event.preventDefault();
            document.getElementById("thanks").style.display = "none";
            document.getElementById("black").style.display = "none";
        };
    }

})();


