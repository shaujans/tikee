$(document).ready(function() {
    var modal = {
        container_register : $("#modal_register"),
        container_login : $("#modal_login"),
        button_register_open : $(".account_login a"),
        button_login_open : $(".account_register a"),
        search_button : $(".header__search input[type=submit]"),

        open_register : function() {
            this.close_login();

            $("#modal_register").show();
        },

        open_login : function() {
            this.close_register();

            $("#modal_login").show();
        },

        close_register : function() {
            $("#modal_register").hide();
        },

        close_login : function() {
            $("#modal_login").hide();
        }
    };

    $(document).on("touchend click", ".account_login a", function(event){
        event.stopPropagation();
        event.preventDefault();

        modal.open_login();
    });

    $(document).on("touchend click", ".account_register a", function(event){
        event.stopPropagation();
        event.preventDefault();

        modal.open_register();
    });

    $(document).on("touchend click", ".modal #show-login", function(event){
        event.stopPropagation();
        event.preventDefault();

        modal.open_login();
    });

    $(document).on("touchend click", ".modal #show-register", function(event){
        event.stopPropagation();
        event.preventDefault();

        modal.open_register();
    });

    $(document).on("touchend click", "#close_login", function(event){
        event.stopPropagation();
        event.preventDefault();

        modal.close_login();
        modal.close_register();
    });

    $(document).on("touchend click", "#close_register", function(event){
        event.stopPropagation();
        event.preventDefault();

        modal.close_login();
        modal.close_register();
    });

    $(document).on("touchend click", ".mobile_show", function(event){
        event.stopPropagation();
        event.preventDefault();

        $("li").toggleClass("show");
    });

    $(document).on("touchend click", "#searchbutton", function(event) {
        if ($("#searchtext").val() != "") {
            var url = $("#searchtext").val().replace(/ /g, "&");
            window.location = "/#/search/" + url;
        }
    });
});
