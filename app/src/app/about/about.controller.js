/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.about')
        .controller('AboutController', AboutController);

    // Inject dependencies into constructor (needed when JS minification is applied).
    AboutController.$inject = [
        // Angular
        '$log'
    ];

    function AboutController(
        // Angular
        $log
    ) {
        // ViewModel
        // =========
        var vm = this;

        // User Interface
        // --------------
        vm.$$ui = {
            classname: 'general'
        }
    }

})();
