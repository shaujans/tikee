/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    let concat = require('gulp-concat');

    gulp.task('vendor', [
        'vendor:angular',
        'vendor:faker',
        'vendor:font-awesome',
        'vendor:lodash'
    ]);

    gulp.task('vendor:angular', () => {
        let extension = isProduction ? 'min.js' : 'js'

        return gulp // JavaScript
            .src([
                `${CONFIG.dir.node}angular/angular.${extension}`,
                `${CONFIG.dir.node}angular-animate/angular-animate.${extension}`,
                `${CONFIG.dir.node}angular-aria/angular-aria.${extension}`,
                `${CONFIG.dir.node}angular-material/angular-material.${extension}`,
                `${CONFIG.dir.node}angular-messages/angular-messages.${extension}`,
                `${CONFIG.dir.node}angular-resource/angular-resource.${extension}`,
                `${CONFIG.dir.node}angular-ui-router/release/angular-ui-router.${extension}`
            ])
            .pipe(concat('angular.js'))
            .pipe(gulp.dest(`${CONFIG.dir.vendor}angular/`));
    });

    gulp.task('vendor:faker', () => {
        return gulp // JavaScript
            .src(`${CONFIG.dir.node}faker/build/build/faker.min.js`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}faker/`));
    });

    gulp.task('vendor:font-awesome', () => {
        gulp // Fonts
            .src(`${CONFIG.dir.node}font-awesome/fonts/*`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}font-awesome/fonts/`));
        gulp // CSS
            .src(`${CONFIG.dir.node}font-awesome/css/*.*.*`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}font-awesome/css/`));

        return gulp;
    });

    gulp.task('vendor:lodash', () => {
        return gulp // JavaScript
            .src(`${CONFIG.dir.node}lodash/lodash.min.js`)
            .pipe(gulp.dest(`${CONFIG.dir.vendor}lodash/`));
    });

})(require('gulp'));
