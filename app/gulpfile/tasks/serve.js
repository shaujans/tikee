/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    let serve       = require('gulp-serve'),
        browserSync = require('browser-sync').create();

    gulp.task('serve:server',
        serve(CONFIG.serve));

    gulp.task('serve:browser-sync', ['watch'], () => {
        browserSync.init(CONFIG.browserSync);
    });

})(require('gulp'));
