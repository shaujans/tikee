/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(() => {
    'use strict';

    const CONFIG = require('../config.json');

    let gulp = require('gulp');

    gulp.task('assets', [
        'assets:app'
    ]);

    gulp.task('assets:app', () => {
        gulp.src(`${CONFIG.dir.src}assets/**/*`)
            .pipe(gulp.dest(`${CONFIG.dir.dest}assets/`));
    });

})();