/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CONFIG = require('../config.json');

    let serve       = require('gulp-serve'),
        browserSync = require('browser-sync').create();

    gulp.task('watch', [
        'watch:templates',
        'watch:styles',
        'watch:scripts'
    ]);

    gulp.task('watch:templates', () => {
        gulp.watch(CONFIG.dir.templates.src, ['markup:templates']).on('change', browserSync.reload);;
    });

    gulp.task('watch:styles', () => {
        gulp.watch(`${CONFIG.dir.src}css/**/*.scss`, ['styles:app']).on('change', browserSync.reload);;
    });

    gulp.task('watch:scripts', () => {
        gulp.watch(`${CONFIG.dir.src}app/**/*.js`, ['scripts:app']).on('change', browserSync.reload);;
    });

})(require('gulp'));
