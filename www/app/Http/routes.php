<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');

// Documentation
Route::get('/', function () { return view('overview.welcome'); });

// Overview
Route::get('/users', function () { return view('overview.users'); });
Route::post('/users', function () { return view('overview.users'); });
Route::get('/events', function () { return view('overview.events'); });
Route::post('/events', function () { return view('overview.events'); });
Route::get('/tickets', function () { return view('overview.tickets'); });
Route::post('/tickets', function () { return view('overview.tickets'); });

// Users
Route::get('/api/users/{user_id}', 'UsersController@getUserById')->where('user_id','[0-9]+');
Route::get('/api/users', 'UsersController@getUsers');

Route::post('/api/users/{user_id}/update', 'UsersController@updateUser')->where('user_id','[0-9]+');
Route::post('/api/users/{user_id}/avatar','UsersController@uploadAvatar')->where('user_id','[0-9]+');

// Login
Route::post('/api/login', 'UsersController@loginUser');
Route::post('/api/register', 'UsersController@registerUser');

// Events
Route::get('/api/events/tickets/user/{user_id}', 'EventsController@getEventsTicketsUsers')->where('user_id','[0-9]+');
Route::get('/api/events/user/{user_id}', 'EventsController@getEventsUser')->where('user_id','[0-9]+');
Route::get('/api/events/search', 'EventsController@search');
Route::get('/api/events/desc', 'EventsController@getEventsDesc');
Route::get('/api/events/{event_id}', 'EventsController@getEventById')->where('event_id','[0-9]+');
Route::get('/api/events', 'EventsController@getEvents');

Route::post('/api/events/add', 'EventsController@addEvent');

// Tickets
Route::get('/api/tickets/user/{user_id}', 'TicketsController@getTicketsUser')->where('user_id','[0-9]+');
Route::get('/api/tickets/{ticket_id}', 'TicketsController@getTicketById')->where('ticket_id','[0-9]+');
Route::get('/api/tickets', 'TicketsController@getTickets');

Route::post('/api/tickets/add', 'TicketsController@createTicket');
