<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ticket;
use DB;

class TicketsController extends Controller
{
    public function getTickets()
    {
        $tickets = DB::table('tickets')
            
            ->get();
        return $tickets;
    }

    public function getTicketById($ticket_id)
    {
        $tickets = DB::table('tickets')
            ->where('id', $id)
            ->get();
        return $tickets;
    }

    public function getTicketsUser($user_id)
    {
        $events = DB::table('tickets')
            ->join('events', 'tickets.event_id', '=', 'events.id')
            ->where('tickets.user_id', $user_id)
            ->get();
        return $events;
    }

    public function createTicket(Request $input)
    {
        $ticket = new Ticket();
        $ticket->event_id = $input['event_id'];
        $ticket->user_id = $input['user_id'];
        $ticket->amount = $input['amount'];
        $ticket->attend = 1;
        $ticket->save();
        return json_encode($ticket);
    }
}
