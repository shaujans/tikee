<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Event extends Authenticatable
{
    public $timestamps = true;
    protected $fillable = ['user_id', 'name', 'datetime', 'time', 'price', 'description', 'address', 'website_url', 'keywords', 'image'];
}
