<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Ticket extends Authenticatable
{
    public $timestamps = true;
    protected $fillable = ['event_id', 'user_id', 'attend'];
}
