<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {

        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
    }
}


class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 1; $i <= 20; $i++)
        {
            DB::table('users')->insert([[
                'username' => $faker->unique()->userName,
                'first_name' => $faker->firstName($gender = null|'male'|'female'),
                'last_name' => $faker->lastName,
                'password' => md5('password'),
                'email' => $faker->unique()->email,
                'avatar' => 'empty_avatar.png'
            ]]);
        }
    }
}

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        for ($i = 1; $i <= 3; $i++)
        {
            DB::table('categories')->insert([[
                'name' => 'name_' . $i,
                'description' => 'description ' . $i
            ]]);
        }
    }
}

class EventsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 1; $i <= 40; $i++)
        {
            DB::table('events')->insert([[
                'user_id' => rand(1, 20),
                'category_id' => rand(1, 3),
                'name' => $faker->unique()->company,
                'datetime' => $faker->date($format = 'Y-m-d', $max = 'now'),
                'time' => $faker->time($format = 'H:i:s', $max = 'now'),
                'price' => rand(0, 500),
                'description' => $faker->text($maxNbChars = 450),
                'address' => $faker->cityPrefix . ' ' . $faker->secondaryAddress,
                'website_url' => $faker->url,
                'keywords' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                'image' => '', // $faker->image('public/pictures', 250, 250) doesnt work :(
                'active' => 1
            ]]);
        }

        for ($i = 1; $i <= 40; $i++)
        {
            $filename = md5(rand(10000, 99999)) . $i . '.jpeg';

            $url = 'https://unsplash.it/350/350/?random';
            $img = 'public/pictures/' . $filename;
            file_put_contents($img, file_get_contents($url));

            $table = DB::table('events')->where('id', '=', $i)->get();
            DB::table('events')
                ->where('id', '=', $i)
                ->update(
                array(
                    'image' => 'pictures/' . $filename
                )
            );
        }
    }
}

class TicketsTableSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 1; $i <= 150; $i++)
        {
            DB::table('tickets')->insert([[
                'user_id' => rand(1, 20),
                'event_id' => rand(1, 40),
                'amount' => rand(1, 20)
            ]]);
        }
    }
}
