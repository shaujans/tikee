<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tikee Backend</title>

        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ URL::asset('styles/reset.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('styles/common.css') }}">
    </head>
    <body>
        <div class="container">
            <aside>
                <h1>Tikee Backend</h1>
                <ul>
                    <a href="/users"><li><i class="fa fa-tags"></i> Users</li></a>
                    <a href="/events"><li><i class="fa fa-calendar"></i> Events</li></a>
                    <a href="/tickets"><li><i class="fa fa-ticket"></i> Tickets</li></a>
                </ul>
            </aside>
            <main>
                @yield('content')
            </main>
        </div>
    </body>
</html>
