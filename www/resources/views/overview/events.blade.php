@extends('overview.app')

@section('content')

<?php

    if (!isset($_COOKIE["loggedIn"]))
    {
        header('Location:/');
        exit;
    }

    if (isset($_POST['user_id']))
    {
        DB::table('events')
            ->where('id', $_POST['id'])
            ->update(
                array(
                    'user_id'     => $_POST['user_id'],
                    'name'        => $_POST['name'],
                    'datetime'    => $_POST['datetime'],
                    'time'        => $_POST['time'],
                    'price'       => $_POST['price'],
                    'description' => $_POST['description'],
                    'address'     => $_POST['address'],
                    'website_url' => $_POST['website_url'],
                    'keywords'    => $_POST['keywords']
                )
            );

        // image upload
        if ($_FILES['image']['name'] != "")
        {
            $file = $_FILES['image'];
            $destinationPath = 'pictures/';

            // Get a unique name
            $filename = md5($_POST['name'] . $_POST['id'] . rand(1000, 9999)) . '.jpg';
            $filename = str_replace(' ', '_', $filename);
            move_uploaded_file($_FILES['image']['tmp_name'], $destinationPath . $filename);

            DB::table('events')
                ->where('id', $_POST['id'])
                ->update(array('image' =>  'pictures/' . $filename));
        }

        header('Location:/events');
    }

    if (isset($_POST['delete_id']) && !isset($_POST['username']))
    {
        DB::table('events')
            ->where('id', $_POST['delete_id'])
            ->update(array('deleted_at' => date('Y-m-d h:i:s')));

        header('Location:/events');
    }

    if (isset($_POST['new_name']))
    {
        DB::table('events')
            ->insert(
                array(
                    'user_id'     => $_POST['new_id'],
                    'name'        => $_POST['new_name'],
                    'datetime'    => $_POST['new_datetime'],
                    'time'        => $_POST['new_time'],
                    'price'       => $_POST['new_price'],
                    'description' => $_POST['new_description'],
                    'address'     => $_POST['new_address'],
                    'website_url' => $_POST['new_website_url'],
                    'keywords'    => $_POST['new_keywords']
                )
            );

        // avatar upload
        if ($_FILES['new_image']['name'] != "")
        {
            $file = $_FILES['new_image'];
            $destinationPath = 'pictures/';

            // Get a unique name
            $filename = md5($_POST['new_name'] . rand(1000, 9999)) . '.jpg';
            $filename = str_replace(' ', '_', $filename);
            move_uploaded_file($_FILES['new_image']['tmp_name'], $destinationPath . $filename);

            DB::table('events')
                ->where('name', $_POST['new_name'])
                ->update(array('image' => $filename));
        }

        header('Location:/users');
    }
?>



    <h1>Create event</h1>
    <table>
            <tr><td>Create</td><td></td></tr>
        {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
            <tr>
                <td>User ID</td>
                <td><?=Form::number('new_id', '', array('required', 'placeholder' => 'User ID'));?></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><?=Form::text('new_name', '', array('required', 'placeholder' => 'Name'));?></td>
            </tr>
            <tr>
                <td>Date</td>
                <td><?=Form::date('new_datetime', '', array('required', 'placeholder' => 'Date'));?></td>
            </tr>
            <tr>
                <td>Time</td>
                <td><?=Form::time('new_time', '', array('required', 'placeholder' => 'Time'));?></td>
            </tr>
            <tr>
                <td>Price</td>
                <td><?=Form::number('new_price', '', array('required', 'placeholder' => 'Price'));?></td>
            </tr>
            <tr>
                <td>Description</td>
                <td><?=Form::text('new_description', '', array('required', 'placeholder' => 'Description'));?></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><?=Form::text('new_address', '', array('required', 'placeholder' => 'Address'));?></td>
            </tr>
            <tr>
                <td>Website URL</td>
                <td><?=Form::text('new_website_url', '', array('required', 'placeholder' => 'Website Url'));?></td>
            </tr>
            <tr>
                <td>Keywords</td>
                <td><?=Form::text('new_keywords', '', array('required', 'placeholder' => 'Keywords'));?></td>
            </tr>
            <tr>
                <td>Image</td>
                <td><?=Form::file('new_image', array('placeholder' => 'Image'));?></td>
            </tr>
            <tr>
                <td></td>
                <td><?=Form::submit('Add');?></td>
            </tr>
        {!! Form::close() !!}
    </table>


    <h1>Events - <a href="/api/events">API</a></h1>
<?php $events = DB::table('events')->where('deleted_at', '=', NULL)->get(); ?>

        <table class="view">
            <tr>
                <td>ID</td>
                <td>User</td>
                <td>Name</td>
                <td>Date</td>
                <td>Time</td>
                <td>Price</td>
                <td>Description</td>
                <td>Address</td>
                <td>Website URL</td>
                <td>Keywords</td>
                <td>Image</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
<?php foreach ($events as $event) : ?>
            {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                <tr>
                    <td style="display: none;"><?=Form::text('id', $event->id, array('required'));?></td>
                    <td><?=$event->id;?></td>
                    <td style="display: none;"><?=Form::text('user_id', $event->id, array('required'));?></td>
                    <td><?=$event->user_id;?></td>
                    <td><?=Form::text('name', $event->name, array('required', 'placeholder' => 'Name'));?></td>
                    <td><?=Form::text('datetime', $event->datetime, array('required', 'placeholder' => 'Date'));?></td>
                    <td><?=Form::text('time', $event->time, array('required', 'placeholder' => 'Time'));?></td>
                    <td><?=Form::text('price', $event->price, array('required', 'placeholder' => 'Price'));?></td>
                    <td><?=Form::text('description', $event->description, array('required', 'placeholder' => 'Description'));?></td>
                    <td><?=Form::text('address', $event->address, array('required', 'placeholder' => 'Address'));?></td>
                    <td><?=Form::text('website_url', $event->website_url, array('required', 'placeholder' => 'Website Url'));?></td>
                    <td><?=Form::text('keywords', $event->keywords, array('required', 'placeholder' => 'Keywords'));?></td>
                    <td><a href="<?=$event->image;?>" target="_blank"><img src="<?=$event->image;?>"></a></td>
                    <td><td><?=Form::file('image', array('placeholder' => 'Image'));?></td></td>
                    <td><?=Form::submit('Update');?></td>
            {!! Form::close() !!}
            {!! Form::open(['method' => 'post', 'enctype' => 'multipart/form-data', 'files' => true]) !!}
                    <td>
                        <p style="display: none;"><?=Form::text('delete_id', $event->id, array('required'));?></p>
                        <?=Form::submit('Delete');?>
                    </td>
            {!! Form::close() !!}
                </tr>
<?php endforeach; ?>
        </table>

@endsection
